<?php

	$root = "../GodIsAGirl/Assets/";
	$webpath = "/GodIsAGirl/Assets/";

	$tileset = [
		"Map/Forest/Tiles/", 
		"Map/Desert/Tiles/", 
		"Map/Graveyard/Tiles/", 
		"Map/Winter/Tiles/", 
		"Map/Sci-fi/Tiles/"
	];

    function display_sprite($folder) {
    	global $root, $webpath;

        $files = array_filter(scandir($folder), function($data) { return preg_match("/\\.(png|jpe?g|gif)$/i", $data); });

        $output = "<h2>".substr($folder, strlen($root))."</h2>";
        foreach ($files as $file)
            $output .= "<img alt=\"\" src=\"".$webpath.substr($folder, strlen($root)).$file."\" class=\"bloc\" />";
        return $output;
    }

    $replace = array("[webAssetPath]" => $webpath);
    $replace["[tileList]"] = "";
    foreach ($tileset as $val)
        $replace["[tileList]"] += display_sprite($root.$val);

    echo $replace(array_keys($replace), array_values($replace), file_get_contents("./index.tpl"));
?>

