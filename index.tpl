<!DOCTYPE html>
<html>
<head>
	<title>Chunk Map Editor: Ghost Runner</title>

    <style>
        @font-face {
            font-family: MotionControl-Bold;
            src: url([webAssetPath]Fonts/MotionControl-Bold.otf)
        }
    </style>

	<script src="./lib/jquery.min.js"></script>
    <link rel="stylesheet" href="./lib/fontawesome/css/font-awesome.min.css" />

    <link rel="stylesheet" href="./lib/bootstrap/css/bootstrap.min.css" />
    <script src="./lib/bootstrap/js/bootstrap.min.js" ></script>

    <link rel="stylesheet" href="./lib/highlight/rainbow.css"/>
    <script src="./lib/highlight/highlight.pack.js"></script>

    <script src="JS/script.js"></script>
    <link rel="stylesheet" href="Css/style.css"/>

</head>
<body>

    <div class="col1 box">
        <div class="content">
            <h1><a href="../../"><i class="fa fa-arrow-circle-o-left" style="margin-right: 10px;"></i></a>Ghost Runner</h1>

            <div class="btn-group">
                <button class="btn btn-success" onclick="AddRowTop()">Row Top</button>
                <button class="btn btn-success" onclick="AddRowBottom()">Row Bottom</button>
            </div>
            <div class="btn-group">
                <button class="btn btn-primary" onclick="AddColLeft()">Col Left</button>
                <button class="btn btn-primary" onclick="AddColRight()">Col Right</button>
            </div>
            <button class="btn btn-danger" onclick="SelectDelete()">Delete</button>
            <button class="btn btn-warning" onclick="getCode()">Code</button>
        </div>

        <div class="content" style="background:inherit">
            <h3 class="fancy"><span onclick="$('#meta').slideToggle()">Map Data</span></h3>
            <div id="meta">
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name"/>
                </div>
                <div class="form-group">
                    <label for="spwn">Chance Of Spawn:</label>
                    <input type="text" class="form-control" id="spwn" value="100"/>
                </div>
                <div class="form-group">
                    <label for="diff">Difficulty:</label>
                    <input type="text" class="form-control" id="diff" value="5.0"/>
                </div>
                <div class="form-group">
                    <label for="off">Row Offset:</label>
                    <input type="text" class="form-control" id="off" value="0"/>
                </div>
            </div>
            <hr/>
        </div>

        <div class="fill">
        	[tileList]
        </div>
    </div>
    <div class="col2">
        <table>
            <tbody>
                <tr><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td></td><td></td><td></td><td></td><td></td></tr>
            </tbody>
        </table>

        <div class="code" style="display:none;">
            <button class="btn" onclick="goBack()" style="float: left; margin-bottom: 20px;">
                <i class="fa fa-arrow-circle-o-left" style="margin-right: 5px;"></i>
                Back To Design
            </button>
            <button class="btn btn-success" onclick="save()" style="float: left; margin-bottom: 20px; margin-left: 5px;">
                <i class="fa fa-floppy-o" style="margin-right: 5px;"></i>
                Save
            </button>
            <div class="end"></div>
            <pre><code class="js"></code></pre>
        </div>
    </div>

    <div class="end"></div>

    <div class="modal fade" id="save-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Trying to save...</h4>
                </div>
                <div class="modal-body">
                    <div style="font-size: 70px; text-align: center;">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>
                    <div class="alert alert-warning">
                        <strong>File Already Exists:</strong> Do you want to overwrite this file ?
                    </div>
                    <div class="alert alert-success">
                        <strong>Success!</strong> Chunk Saved
                    </div>
                    <div class="alert alert-danger">
                        <strong>Error :</strong> Indicates a dangerous or potentially negative action.
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success">Change Name</button>
                    <button type="button" class="btn btn-danger">Overwrite File</button>
                </div>
            </div>
        </div>
    </div>

</body>
</html>