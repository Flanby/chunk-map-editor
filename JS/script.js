var row = 5, col = 5, selected = null;

function AddColRight() {
    var child = $("table tbody")[0].children;
    for (var i = 0; i < child.length; i++)
        child[i].append(document.createElement("td"));
    SetOnclickOnTd();
    col++;
}

function AddColLeft() {
    var child = $("table tbody")[0].children;
    for (var i = 0; i < child.length; i++)
        $(child[i]).prepend(document.createElement("td"));
    SetOnclickOnTd();
    col++;
}

function AddRowBottom() {
    var newrow = document.createElement("tr")
    for (var i = 0; i < col; i++)
        newrow.append(document.createElement("td"));
    $("table tbody")[0].append(newrow);
    SetOnclickOnTd();
    row++;
}

function AddRowTop() {
    var newrow = document.createElement("tr")
    for (var i = 0; i < col; i++)
        newrow.append(document.createElement("td"));
    $("table tbody").prepend(newrow);
    SetOnclickOnTd();
    row++;
}

function SelectNewTile() {
    $(selected).removeClass("TileSelect");
    selected = this;
    $(selected).addClass("TileSelect");            
}

function SelectDelete() {
    $(selected).removeClass("TileSelect");
    selected = null;
}

function SetOnclickOnTd() {
    $("table tbody td").each(function(index, elem) { elem.onclick=pushTile; });
}

function pushTile() {
    if (selected == null)
        this.style.backgroundImage = "";
    else
        this.style.backgroundImage = "url(\""+selected.src+"\")";
}

$(document).ready(function() {
    $(".bloc").each(function(index, elem) { elem.onclick=SelectNewTile; });
    SetOnclickOnTd();
});

var dump;
var baseURI = "";
function getCode() {
    var table = [];
    var tilenbr = 0;
    baseURI = "";
    
    // Création Table
    $("td").each(function(index, elem) {
        if (index % col == 0)
            table.push([]);
        table[Math.floor(index / col)].push(elem.style.backgroundImage);
    });

    // Refactor URL
    table.forEach(function(elem, y) {
        elem.forEach(function(cell, x) {
            if (cell == "")
                return ;
            if (baseURI == "")
                baseURI = /^url\(\"(.*)Assets\/.*\"\)/gim.exec(cell)[1];
            this[x] = decodeURIComponent(/(Assets\/.*)\"\)/gim.exec(cell)[1]);
            tilenbr++;
        }, table[y]);
    });

    // Find Best Square
    var blocs = [], workingTable = table, zero = {x: 0, y: 0, w: 0, h: 0}, max = zero;
    while (tilenbr != 0) {
        for (var y = 0; y < row; y++) {
            for (var x = 0; x < col; x++) {
                if (workingTable[y][x] == "")
                    continue;
                
                var tmp = checksquare(workingTable, x, y, 1, 1);
                if (tmp.w * tmp.h > max.w * max.h)
                    max = tmp;
            }
        }

        tilenbr -= max.w * Math.ceil(max.h);
        max.tiles = [];
        for (var y = max.y; y < max.y + max.h; y++)
            for (var x = max.x; x < max.x + max.w; x++) {
                max.tiles.push(workingTable[y][x]);
                workingTable[y][x] = "";
            }
        blocs.push(max);
        
        max = zero;
    }

    blocs.sort(function(x, y) { return x.x - y.x; });
    
    dump = {name: $("#name").val(), row: row, col: col, blocs: blocs, rowOffset: parseInt($("#off").val()), ChanceOfSpawn: parseInt($("#spwn").val()), Difficulty: parseFloat($("#diff").val())};

    $(".js").html(JSON.stringify(dump, null, 4));
    $('.js').each(function(i, block) {
        hljs.highlightBlock(block);
    });
    
    $("table").hide();
    $(".code").show();
}

function goBack() {
    $("table").show();
    $(".code").hide();
}

function checksquare(wtable, x, y, sizex, sizey, halfbloc = false) {
    // Check Limit
    if (x + sizex > col || y + sizey > row)
        return null;
    // Max Width
    if (sizex > 6)
        return null;

    var myImage = new Image();
    
    for (var ny = 0; ny < sizey; ny++)
        if (wtable[y + ny][x + sizex - 1] == "")
            return null;
        else {
            myImage.src = baseURI + wtable[y + ny][x + sizex - 1];
            if (myImage.width != myImage.height) {
                if (!halfbloc && (sizex != 1 || sizey != 1))
                    return null;
                halfbloc = true;
            }
            else if (halfbloc)
                return null;
        }
        
    for (var nx = 0; nx < sizex; nx++)
        if (wtable[y + sizey - 1][x + nx] == "")
            return null;
        else {
            myImage.src = baseURI + wtable[y + sizey - 1][x + nx];
            if (myImage.width != myImage.height) {
                if (!halfbloc && (sizex != 1 || sizey != 1))
                    return null;
                halfbloc = true;
            }
            else if (halfbloc)
                return null;
        }

    var testx = checksquare(wtable, x, y, sizex + 1, sizey, halfbloc);
    var testy = halfbloc ? null : checksquare(wtable, x, y, sizex, sizey + 1, halfbloc);

    if (halfbloc) {
        myImage.src = baseURI + wtable[y][x];
        sizey = myImage.height / myImage.width;
    }

    if (testx == null && testy == null)
        return {x: x, y: y, w: sizex, h: sizey};
    if (testy == null)
        return testx;
    if (testx == null)
        return testy;
    return (testx.w * testx.h > testy.w * testy.h ? testx : testy);
}

function buildHtml(data) {
    if (typeof data == "string")
        return data;

    if (!data.hasOwnProperty("type"))
        return "";

    var obj = document.createElement(data.type);
    if (data.hasOwnProperty("attr"))
        $.each(data.attr, function (attr, value) {
            $(obj).attr(attr, value);
        });
    
    if (data.hasOwnProperty("content"))
        data.content.forEach(function (elem) {
            $(obj).append(buildHtml(elem))
        });
        
    return obj;
}

function changeModal(body, foot) {
    if (typeof body === "string")
        $('#save-modal .modal-body').html(body);
    else {
        $('#save-modal .modal-body').html("");
        body.forEach(function(elem) {
            $('#save-modal .modal-body').append(buildHtml(elem));
        });
    }

    if (typeof foot === "string")
        $('#save-modal .modal-footer').html(foot);
    else {
        $('#save-modal .modal-footer').html("");
        foot.forEach(function(elem) {
            $('#save-modal .modal-footer').append(buildHtml(elem));
        });
    }
}

function save() {
    $('#save-modal').modal('show');
    //changeModal("<div style=\"font-size: 70px; text-align: center;\"><i class=\"fa fa-refresh fa-spin\"></i></div>", "");

    changeModal([
        {
            type: "div",
            attr: {
                style: "font-size: 70px; text-align: center;"
            },
            content: [
                {
                    type: "i",
                    attr: {
                        class: "fa fa-refresh fa-spin"
                    }
                }
            ]
        }
    ], 
    "");

    $.post( "../MapChunks/NewJsonChunk.php", function(data) {
        alert( "success" );
    })
    .fail(function(jqXHR) {
        //console.log(jqXHR, textStatus, errorThrown);
        changeModal([
            {
                type: "div",
                attr: {
                    class:"alert alert-danger"
                },
                content: [{
                        type: "strong",
                        content: ["Error "+jqXHR.status+": "]
                    },
                    jqXHR.statusText]
            }
        ], 
        [
            {
                type: "button",
                attr: {
                    type:"button",
                    class:"btn btn-primary",
                    "data-dismiss":"modal"
                },
                content: ["Close"]
            }
        ]);
    });
    //.always(function() {
    //    alert( "finished" );
    //});
}